package main

import (
	"fmt"
	"log"
	"os"
	// "path/filepath"
)

func splitPath(filePth string) {
	// Напишите код, который выведет следующее
	// filename: <name>
	// extension: <extension>

	// Подсказка. Возможно вам понадобится функция strings.LastIndex
	// Для проверки своего решения используйте функции filepath.Base() filepath.Ext(
	// ) Они могут помочь для проверки решения

	// Домашнее задание
	// На основе шаблона дописать код, который выводит имя файла и
	// расширение по полному пути до файла.
	// Например, мы запускаем программу и передаем в качестве аргумента
	// путь к файлу в формате /usr/local/file.txt. Программа должна вывести
	// отдельно file и отдельно txt

	var fileName, fileExt string
	var lastSlashIndex, lastDotIndex int = -1, -1 //

	for idx, val := range filePth {
		if string(val) == "/" {
			lastSlashIndex = idx
		}
		if string(val) == "." {
			lastDotIndex = idx
		}
	}

	if lastDotIndex > lastSlashIndex {
		fileName = filePth[lastSlashIndex+1 : lastDotIndex]
		fileExt = filePth[lastDotIndex+1:]
	} else {
		fileName = filePth[lastSlashIndex+1:]
	}

	fmt.Println("Ответ:")
	fmt.Printf("filename: %s\n", fileName)
	fmt.Printf("extension: %s\n", fileExt)

}

func main() {
	if len(os.Args) < 2 {
		log.Fatal("Укажите полный путь до файла вторым аргументом")
	}

	filePth := os.Args[1]
	splitPath(filePth)

	// // для тестов
	// testPaths := []string{"file1.txt", "file", "/newfile.txt",
	// 	"./filename-ext /home/robotomize/1.txt",
	// 	"./filename-ext /home/robotomize/1.txt.txt",
	// 	"./filename-ext /home/robotomize/1"}

	// for _, path := range testPaths {
	// 	fmt.Printf("Путь: %s\n", path)
	// 	splitPath(path)
	// 	fmt.Println()
	// }

}
